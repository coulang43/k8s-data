FROM node:21-alpine AS build
WORKDIR /app
COPY src/my-app/. .
RUN npm install
RUN npm run build

EXPOSE 80

# Serve Application using Nginx Server
FROM nginx:alpine
COPY --from=build /app/dist/my-app/browser/. /usr/share/nginx/html